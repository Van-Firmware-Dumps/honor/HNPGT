## magic-user 15 AP3A.240905.015.A2 eng.root.00000000.000000 test-keys
- Manufacturer: qualcomm
- Platform: kalama
- Codename: kalama
- Brand: qti
- Flavor: magic-user
- Release Version: 15
- Kernel Version: 5.15.148
- Id: AP3A.240905.015.A2
- Incremental: eng.root.20241107.223228
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: qti/kalama/kalama:13/TKQ1.230829.001/root11072230:user/release-keys
- OTA version: 
- Branch: magic-user-15-AP3A.240905.015.A2-eng.root.00000000.000000-test-keys-2870
- Repo: qti/kalama
